<?php

namespace App\Controllers;
use App\Models\FormLeadModel;

class DefaultController extends BaseController
{
	public function index()
	{
		return view('home');
	}

	public function formValidation() 
	{
		if (!$this->request->isAjax())
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}
		else
		{
			helper(['form', 'url, validation']);

			$formModel = new FormLeadModel();
			$validation = \Config\Services::validation();
			
			$response = [];
			$fields = [];

			$valid = $validation->withRequest($this->request)->setRules($formModel->validationRules)->run();

			foreach($this->request->getPost() as $field => $value)
			{
				if (array_key_exists($field, $formModel->validationRules))
				{
					$fields[] = array('field'=>$field, 'error'=>$validation->hasError($field), 'message'=> $validation->getError($field));
				}
			}

			if ($valid) 
			{
				$saveResults = $formModel->save([
					'name' => $this->request->getVar('name'),
					'email'  => $this->request->getVar('email'),
					'phone'  => $this->request->getVar('phone'),
					'message' => $this->request->getVar('message'),
				]);

				$response['save_results'] = $saveResults;

				$email = \Config\Services::email();
				
				$email->setFrom($this->request->getVar('email'), $this->request->getVar('name'));
				$email->setTo('guy-smiley@example.com');
				$email->setSubject('Web contact form submission');
				$email->setMessage($this->generateMessage($this->request->getPost()));
				$email->send();
				
				$response['message'] = '<p>Your request has been sent.</p>';
			}

			$response['fields'] = $fields;
			$response['status'] = $valid ? 'ok' : 'error';
			$response['errorcount'] = count($validation->getErrors());
			
			//regen csrf tokens for re-submission
			$response['csrfName'] = csrf_token();
        	$response['csrfHash'] = csrf_hash();

			return $this->response->setJSON($response);
		}
    }

	private function generateMessage($data)
	{
		$message = "<p><b>A contact for submission was received</b></p>";
		$message .= "<p><b>Name</b>: " . $data['name']."<br />";
		$message .= "<b>Email Address</b>: " . $data['email']."<br />";
		$message .= "<b>Phone:</b> " . $data['phone']."<br />";
		$message .= "<b>Message:</b>" . $data['message'] . "</p>";

		return $message;
	}
}
