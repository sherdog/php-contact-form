<?php 
namespace App\Models;
use CodeIgniter\Model;

class FormLeadModel extends Model
{
    protected $table = 'form_leads';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;

    protected $allowedFields = ['name', 'email', 'phone', 'message'];

    public $validationRules = [
        'name' => [
            'label' => 'Name',
            'rules' => 'required|min_length[3]',
            'errors' => [
                'required' => '{field} is required',
            ]
        ],
        'email' => [
            'label' => 'Email Address',
            'rules' => 'required|valid_email',
            'errors' => [
                'required' => '{field} is required',
                'valid_email' => 'Please enter a valid email'
            ]
        ],
        'message' => [
            'label' => 'Message',
            'rules' => 'required',
            'errors' => [
                'required' => '{field} is required'
            ]
        ]
    ];
}