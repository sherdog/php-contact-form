<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class WebLeads extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'name'       => [
					'type'       => 'VARCHAR',
					'constraint' => '128',
			],
			'email'       => [
					'type'       => 'VARCHAR',
					'constraint' => '96',
			],
			'phone' => [
					'type' => 'VARCHAR',
					'constraint' => '12',
					'null' => true,
			],
			'message' => [
					'type' => 'TEXT',
			],
			
	]);
	$this->forge->addKey('id', true);
	$this->forge->createTable('form_leads');
	}

	public function down()
	{
		$this->forge->dropTable('form_leads');
	}
}
