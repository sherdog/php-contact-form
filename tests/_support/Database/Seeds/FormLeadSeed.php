<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class FormLeadSeed extends Seeder
{
        public function run()
        {
                $data = [
                        'name' => 'darth vader',
                        'email'    => 'darth@theempire.com',
                        'phone'     => '555-555-5555',
                        'message' => 'This is just a test message for testing purposes.'
                ];

                $this->db->table('form_leads')->insert($data);
        }
}