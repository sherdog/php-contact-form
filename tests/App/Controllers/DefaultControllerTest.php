<?php

namespace App\Controllers;

use App\Models\FormLeadModel;
use CodeIgniter\Test\DatabaseTestTrait;
use CodeIgniter\Test\FeatureTestTrait;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\ControllerTestTrait;

class DefaultControllerTest extends CIUnitTestCase
{
    use DatabaseTestTrait;
    use FeatureTestTrait;

    protected $refresh  = true;
    protected $seed     = 'FormLeadSeed';
    protected $testData;
    protected $setUpMethods = [
        'mockEmail',
    ];
    
    public function setUp(): void
    {
        $this->testData = [
            'name'  => 'Test User',
            'email' => 'testuser@example.com',
            'phone' => '555-555-5555',
            'message' => "This is another test checking save"
        ];

        parent::setUp();
    }

    public function testLoadHomepage()
    {
        $routes = [[ 'get', '/', 'DefaultController::index' ]];
        $result = $this->withRoutes($routes)
                  ->get('/');

        $this->assertTrue($result->isOK());
    }

    public function testFormSubmission()
    {
        $email = \Config\Services::email();
        $validator = \Config\Services::validation();
        
        $model = new FormLeadModel();

        $this->assertTrue($validator->setRules($model->validationRules)->run($this->testData));
        $this->assertTrue($model->save($this->testData));
        $this->seeInDatabase('form_leads', [$this->testData['email']]);

        $email->setFrom($this->testData['email'], $this->testData['name']);
        $email->setTo('guy-smiley@example.com');
        $email->setSubject('Web contact form submission');
        $email->setMessage($this->generateMessage($this->testData));

        $this->assertTrue($email->send());
    }

    private function generateMessage($data)
    {
        $message = "<p><b>A contact for submission was received</b></p>";
        $message .= "<p><b>Name</b>: " . $data['name']."<br />";
        $message .= "<b>Email Address</b>: " . $data['email']."<br />";
        $message .= "<b>Phone:</b> " . $data['phone']."<br />";
        $message .= "<b>Message:</b>" . $data['message'] . "</p>";

        return $message;
    }
}